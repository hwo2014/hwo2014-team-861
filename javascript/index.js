var requirejs = require('requirejs');
requirejs.config({
    nodeRequire: require
});

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var track = process.argv[6];
var carCount = new Number(process.argv[7]);
if (isFinite(carCount) === false) {
    carCount = 1;
}
var password = process.argv[8];

requirejs(['DOGmodules/gameClient',
           'DOGmodules/logger',
           'DOGmodules/ai'],
   function(client) {
       client.initialize(serverHost, serverPort, botName, botKey, track, carCount, password);
   }
);